//
//  SentenceTableViewCell.h
//  Audify
//
//  Created by CHIU on 2015-11-14.
//  Copyright © 2015 Audify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface SentenceTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *numLabel;

@property (weak, nonatomic) IBOutlet UILabel *sentenceLabel;


@end
