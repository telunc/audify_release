//
//  IntroViewController.m
//  Audify
//
//  Created by Apple on 2015-11-14.
//  Copyright © 2015 Audify. All rights reserved.
//

#import "IntroViewController.h"
#import "TabBarViewController.h"

@interface IntroViewController ()

@end

@implementation IntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)Start:(id)sender {
    TabBarViewController *tab = [[TabBarViewController alloc] init];
    [self presentViewController: tab animated: NO completion: nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
