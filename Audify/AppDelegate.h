//
//  AppDelegate.h
//  Audify
//
//  Created by Apple on 2015-11-14.
//  Copyright © 2015 Audify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

