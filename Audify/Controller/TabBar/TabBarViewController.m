//
//  TabBarViewController.m
//  Audify
//
//  Created by Apple on 2015-11-14.
//  Copyright © 2015 Audify. All rights reserved.
//

#import "TabBarViewController.h"
#import "WebViewController.h"
#import "ImageViewController.h"

@interface TabBarViewController ()

@end

@implementation TabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //Image
    ImageViewController *imageVC = [ImageViewController alloc];
    imageVC.tabBarItem.title = @"Camera";
    imageVC.tabBarItem.image = [UIImage imageNamed:@"cam.pgn"];
    imageVC = [imageVC init];
    
    //Web
    WebViewController *webVC = [[WebViewController alloc] init];
    //UINavigationController *webNav = [[UINavigationController alloc] initWithRootViewController:webVC];
    webVC.tabBarItem.title = @"Web";
    webVC.tabBarItem.image = [UIImage imageNamed:@"web.png"];
    self.viewControllers = @[webVC, imageVC];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
