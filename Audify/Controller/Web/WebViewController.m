//
//  WebViewController.m
//  Audify
//
//  Created by Apple on 2015-11-14.
//  Copyright © 2015 Audify. All rights reserved.
//

#import "WebViewController.h"
#import "SentenceTableViewController.h"

@interface WebViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation WebViewController
@synthesize searchBar;

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [searchBar resignFirstResponder];
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    searchBar.delegate = self;
    
    // Do any additional setup after loading the view from its nib.
    
    NSString *urlAddress = @"en.wikipedia.org/wiki/Computer_science";
    //NSURL *url = [NSURL URLWithString:urlAddress];
    NSURL *url;
    
    if ([urlAddress.lowercaseString hasPrefix:@"http://"]){
        url = [NSURL URLWithString: urlAddress];
    }
    else{
        url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",urlAddress]];
    }
    
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:requestObj];
    [self.view addSubview:_webView];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)search:(id)sender {
    
    NSString *urlAddress = searchBar.text;
    //NSURL *url = [NSURL URLWithString:urlAddress];
    NSURL *url;
    
    if ([urlAddress.lowercaseString hasPrefix:@"http://"]){
        url = [NSURL URLWithString: urlAddress];
    }
    else{
        url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",urlAddress]];
    }
    
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:requestObj];
    [self.view addSubview:_webView];
    
}
- (IBAction)text:(id)sender {
    
    NSString *html = [_webView stringByEvaluatingJavaScriptFromString:
                      @"document.body.innerHTML"];
    
    NSAttributedString* attributedText = [[NSAttributedString alloc] initWithData:[html dataUsingEncoding:NSUTF8StringEncoding]
                                                                          options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                    NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                               documentAttributes:nil
                                                                            error:nil];
    NSString* plainText = [attributedText string];
    
    NSLog(@"%@", plainText);
    
    /*
    SentenceTableViewController *st = [[SentenceTableViewController alloc]init];
    st.content = plainText;
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:st];*/
    
    
    SentenceTableViewController *viewController = [SentenceTableViewController alloc];
    viewController.content= plainText;
    viewController = [viewController init];
    UINavigationController *chatVC = [[UINavigationController alloc]initWithRootViewController:viewController];
    [self presentViewController: chatVC animated: YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
