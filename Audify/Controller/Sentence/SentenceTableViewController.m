//
//  SentenceTableViewController.m
//  Audify
//
//  Created by CHIU on 2015-11-14.
//  Copyright © 2015 Audify. All rights reserved.
//

#import "SentenceTableViewController.h"
#import "SentenceTableViewCell.h"
#import "TabBarViewController.h"

#import <QuartzCore/QuartzCore.h>

#define SPLIT_SYMBOL @"."
#define CELL_REUSEABLE_NAME @"SentenceTableViewCell"

#define PLAY_TEXT @"Play"
#define PAUSE_TEXT @"Pause"
#define STOP_TEXT @"Stop"
#define RESUME_TEXT @"Resume"

@interface SentenceTableViewController () <UITableViewDataSource,UITableViewDelegate, AVSpeechSynthesizerDelegate>

@property (strong, nonatomic) AVSpeechSynthesizer *synthesizer;


@property (weak, nonatomic) IBOutlet UIButton *playAll;
@property (weak, nonatomic) IBOutlet UIButton *stop;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SentenceTableViewController {
    NSMutableArray *sentences;
    NSString *selectedString;
    BOOL isPlaying;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 6, 67, 23);
    UIImageView *backImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"navigator_btn_back"]];
    backImg.frame = CGRectMake(-10, 0, 22, 22);
    [backBtn addSubview:backImg];
    UILabel *backText = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 65, 22)];
    backText.text = @"Back";
    backText.font = [UIFont systemFontOfSize:15];
    [backText setBackgroundColor:[UIColor clearColor]];
    [backText setTextColor:[UIColor blackColor]];
    [backBtn addSubview:backText];
    [backBtn addTarget:self action:@selector(leftBarButtonItemPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    [self.navigationItem setLeftBarButtonItem:leftButton];
    
   
    //self.navigationController.navigationItem.leftBarButtonItem = doneButton;
    
    // delegate that do text to speech
    self.synthesizer = [[AVSpeechSynthesizer alloc] init];
    self.synthesizer.delegate = self;
    
    [self loadInitialData];
    [self configureButtons];
    
    [self.tableView registerNib:[UINib nibWithNibName:CELL_REUSEABLE_NAME bundle:nil] forCellReuseIdentifier:CELL_REUSEABLE_NAME];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // Set up dynamic cell height
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 58.0;
}

- (void) leftBarButtonItemPressed:(id)sender{
    TabBarViewController *web = [[TabBarViewController alloc]init];
    
    [self presentViewController: web animated:NO completion:nil];
}

-(void)playAllButtonPressed:(id)sender{
    isPlaying = true;
    
    // To avoid playing multiple times
    if (!self.synthesizer.speaking) {
        [self playWithSentence:self.content];
    }
    if ([_playAll.titleLabel.text  isEqualToString: PAUSE_TEXT]){
        
        //pause the audio
        [self.synthesizer pauseSpeakingAtBoundary:AVSpeechBoundaryImmediate];
        isPlaying = false;
    
    }
    
    else if([_playAll.titleLabel.text isEqualToString:RESUME_TEXT]){
        //continue playing
        [self.synthesizer continueSpeaking];
        isPlaying = true;
    }
    if(isPlaying){
        [_playAll setTitle:PAUSE_TEXT forState:UIControlStateNormal];
    }else{
        [_playAll setTitle:RESUME_TEXT forState:UIControlStateNormal];
    }
}

-(void)stopButtonPressed:(id)sender{
    isPlaying = false;
    [_playAll setTitle:PLAY_TEXT forState:UIControlStateNormal];
    [self.synthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
}

-(void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance {
    [self.synthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
    [_playAll setTitle:PLAY_TEXT forState:UIControlStateNormal];
    isPlaying = false;
    NSLog(@"Playback finished");
}

- (void)loadInitialData {
    sentences = [[NSMutableArray alloc] init];
    //self.content = @"One reason people lie is to achieve personal power. Yes. No. hhhhhh";
    [self parseSentenceWithContent:self.content];
    isPlaying = false;
}

- (void)parseSentenceWithContent: (NSString *)content {
    NSArray *original_sentences = [content componentsSeparatedByString:SPLIT_SYMBOL];

    // remove all white spaces for before/after each sentence
    for (NSString *s in original_sentences) {
        NSString *trimmedString = [s stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceCharacterSet]];
        [sentences addObject:trimmedString];
    }
}

- (void)configureButtons {
    // set up border and colour
    [[self.playAll layer] setBorderWidth:1.5f];
    [[self.playAll layer] setBorderColor:[UIColor blackColor].CGColor];
    
    // set up border and colour
    [[self.stop layer] setBorderWidth:1.5f];
    [[self.stop layer] setBorderColor:[UIColor blackColor].CGColor];
    
    [_playAll addTarget:self action:@selector(playAllButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [_stop addTarget:self action:@selector(stopButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (sentences) {
        return sentences.count;
    } else {
        return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SentenceTableViewCell *cell = (SentenceTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CELL_REUSEABLE_NAME forIndexPath:indexPath];
    
    [cell.sentenceLabel setText: [sentences objectAtIndex: indexPath.row]];
    
    NSString* sentenceNum = [NSString stringWithFormat:@"%d", indexPath.row+1];
    [cell.numLabel setText: sentenceNum];
    
    return cell;
}

- (void)playWithSentence: (NSString*)sentence {
    if(sentence!= NULL){
        //play sound here
        AVSpeechUtterance *utterance = [[AVSpeechUtterance alloc] initWithString:sentence];
        [self.synthesizer speakUtterance: utterance];
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //selectedString = [sentences objectAtIndex: indexPath.row];
    SentenceTableViewCell *cell =[tableView cellForRowAtIndexPath:indexPath];
    selectedString = cell.sentenceLabel.text;
    NSLog(@"string is: %@\n", selectedString);
    [self playWithSentence: selectedString];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
