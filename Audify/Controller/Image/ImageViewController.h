//
//  ImageViewController.h
//  Audify
//
//  Created by Apple on 2015-11-14.
//  Copyright © 2015 Audify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TesseractOCR/TesseractOCR.h>

@interface ImageViewController : UIViewController <G8TesseractDelegate,
UIImagePickerControllerDelegate,
UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIImageView *imageToRecognize;
@property (strong, nonatomic) UIImagePickerController *imagePicker;
@property (strong, nonatomic) UIImage *chosenImage;


@end
